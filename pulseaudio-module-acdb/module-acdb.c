/***
Module_acdb.c -- Pulseaudio module acdb is a module
belongs to pulseaudio, which could be loaded by pulseaudio.It used to
send audio calibration data when audio usecase setup.

Copyright (c) 2016, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

#include <pulsecore/pulsecore-config.h>
#include <pulsecore/macro.h>
#include <pulsecore/module.h>
#include <pulsecore/modargs.h>
#include <pulsecore/log.h>
#include <pulsecore/core-util.h>

#include <stdio.h>
#include "module-acdb.h"

PA_MODULE_AUTHOR("AGL audio");
PA_MODULE_DESCRIPTION("Load Audio Calibration Data When Audio Usecase Setup");
PA_MODULE_VERSION(PACKAGE_VERSION);
PA_MODULE_LOAD_ONCE(false);

static const char* const valid_modargs[] = {
        NULL
};

struct userdata{
    pa_core *core;
    pa_module *module;
    pa_modargs *modargs;
    pa_hook_slot *sink_input_put_hook_slot,
                 *source_output_put_hook_slot;
    void *acdb_handle;
    acdb_init_t acdb_init;
    acdb_send_audio_cal_t acdb_send_audio_cal;
    acdb_deallocate_t acdb_deallocate;
};

static usecase_type_t get_usecase_type_id(const char* role, int capability){
    usecase_type_t usecase_id;

    if (pa_streq(role, "music") || pa_streq(role, "event"))
        usecase_id = PCM_PLAYBACK;
    else if (pa_streq(role, "navi"))
        usecase_id = PCM_PLAYBACK_DRIVER_SIDE;
    else if (pa_streq(role, "voice_rec"))
        usecase_id = PCM_CAPTURE;
    else if (pa_streq(role, "voice_call"))
        usecase_id = VOICE_CALL;
    else if (pa_streq(role, "hfp_uplink"))
        usecase_id = HFP_UPLINK;
    else if (pa_streq(role, "hfp_downlink"))
        usecase_id = HFP_DOWNLINK;
    else if (pa_streq(role, "hfp_wb_uplink"))
        usecase_id = HFP_WB_UPLINK;
    else if (pa_streq(role, "hfp_wb_downlink"))
        usecase_id = HFP_WB_DOWNLINK;
    else if (pa_streq(role, "icc_call"))
        usecase_id = ICC_CALL;
    else if (pa_streq(role, "sdars_tuner_local_amp"))
        usecase_id = SDARS_TUNER_LOCAL_AMP;
    else{
        if (capability == ACDB_DEV_TYPE_OUT) {
            pa_log_info("%s:Using default PLAYBACK usecase type.",__func__);
            usecase_id = PCM_PLAYBACK;
        }
        else {
            /* capability == ACDB_DEV_TYPE_OUT */
            pa_log_info("%s:Using default CAPTURE usecase type.",__func__);
            usecase_id = PCM_CAPTURE;
        }
    }

    return usecase_id;

}

static int get_acdb_device_id(usecase_type_t usecase_id, int capability){
    int acdb_dev_id = 0;

    if (capability == ACDB_DEV_TYPE_OUT) {
        switch(usecase_id){
            case PCM_PLAYBACK:
            case SDARS_TUNER_LOCAL_AMP:
                acdb_dev_id = 41;
                break;
            case HFP_UPLINK:
                acdb_dev_id = 21;
                break;
            case HFP_DOWNLINK:
            case HFP_WB_DOWNLINK:
                acdb_dev_id = 15;
                break;
            case HFP_WB_UPLINK:
                acdb_dev_id = 39;
                break;
            case VOICE_CALL:
                acdb_dev_id = 94;
                break;
            case ICC_CALL:
                acdb_dev_id = 16;
                break;
            case PCM_PLAYBACK_DRIVER_SIDE:
                acdb_dev_id = 14;
                break;
            default:
                pa_log("%s: no acdb id supported for usecase type (%d)", __func__, usecase_id);
              }
    }
    else {
        /* capability == ACDB_DEV_TYPE_IN */
        switch(usecase_id){
            case PCM_CAPTURE:
            case HFP_UPLINK:
            case HFP_WB_UPLINK:
                acdb_dev_id = 11;
                break;
            case HFP_DOWNLINK:
                acdb_dev_id = 20;
                break;
            case HFP_WB_DOWNLINK:
                acdb_dev_id = 38;
                break;
            case VOICE_CALL:
                acdb_dev_id = 95;
                break;
            case ICC_CALL:
                acdb_dev_id = 13;
                break;
            default:
                pa_log("%s: no acdb id supported for usecase type (%d)", __func__, usecase_id);
        }
    }

    return acdb_dev_id;
}

static pa_hook_result_t sink_input_put_hook_callback(pa_core *c, pa_sink_input *sink_input, struct userdata *udata){
    struct userdata *my_data = (struct userdata *)udata;
    int acdb_dev_id = 0;
    audio_usecase usecase;
    const char *role;
    int capability = ACDB_DEV_TYPE_OUT;
    int app_id = APP_ID_OUT;

    pa_log_info("%s: SINK_INPUT_PUT hook fired", __func__);

    /* get sample rate */
    usecase.sample_rate = sink_input->sample_spec.rate;
    pa_log_debug("%s: sample rate is (%d)", __func__, usecase.sample_rate);

    /* get media role */
    role = pa_proplist_gets(sink_input->proplist, PA_PROP_MEDIA_ROLE);
    if(!role)
        role = "none";
    pa_log_debug("%s: Media role is (%s)", __func__, role);

    if (pa_streq(role, "abstract")){
        pa_log_info("Got abstract role, skip it.");
        return PA_HOOK_OK;
    }

    /* figure out usecase.type */
    usecase.type = get_usecase_type_id(role, capability);
    pa_log_debug("%s: usecase type is (%d)", __func__, usecase.type);

    /* get acdb id */
    acdb_dev_id = get_acdb_device_id(usecase.type, capability);
    pa_log_debug("%s: acdb id is (%d)", __func__, acdb_dev_id);

    /* send payload to acdbloader */
    pa_log_info("%s: sending audio calibration for usecase type (%d), acdb_id is (%d)", __func__, usecase.type, acdb_dev_id);
    my_data->acdb_send_audio_cal(acdb_dev_id, capability, app_id, usecase.sample_rate);

    return PA_HOOK_OK;
}

static pa_hook_result_t source_output_put_hook_callback(pa_core *c, pa_source_output *source_output, struct userdata *udata){
    struct userdata *my_data = (struct userdata *)udata;
    int acdb_dev_id = 0;
    audio_usecase usecase;
    const char *role;
    int capability = ACDB_DEV_TYPE_IN;
    int app_id = APP_ID_IN;

    pa_log_info("%s: SOURCE_OUTPUT_PUT hook fired", __func__);

    /* get sample rate */
    usecase.sample_rate = source_output->sample_spec.rate;
    pa_log_debug("%s: sample rate is (%d)", __func__, usecase.sample_rate);

    /* get media role */
    role = pa_proplist_gets(source_output->proplist, PA_PROP_MEDIA_ROLE);
    if(!role)
        role = "none";
    pa_log_debug("%s: Media role is (%s)", __func__, role);

    if (pa_streq(role,"abstract")){
        pa_log_info("Got abstract role, skip it.");
        return PA_HOOK_OK;
    }

    /* The capture path for the SDARS tuner does not require calibration since
     * there will be no PP blocks used in the DSP.
     */
    if (pa_streq(role,"sdars_tuner_local_amp")){
        pa_log_info("Got sdars_tuner_local_amp role, skip it.");
        return PA_HOOK_OK;
    }

    /* figure out usecase.type */
    usecase.type = get_usecase_type_id(role, capability);
    pa_log_debug("%s: usecase type is (%d)", __func__, usecase.type);

    /* get acdb id */
    acdb_dev_id = get_acdb_device_id(usecase.type, capability);
    pa_log_debug("%s: acdb id is (%d)", __func__, acdb_dev_id);

    /* send payload to acdbloader */
    pa_log_info("%s: sending audio calibration for usecase type (%d), acdb_id is (%d)", __func__, usecase.type, acdb_dev_id);
    my_data->acdb_send_audio_cal(acdb_dev_id, capability, app_id, usecase.sample_rate);

    return PA_HOOK_OK;
}

int pa__init(pa_module* m)
{
    pa_modargs *ma = NULL;
    struct userdata *u;
    int ret_acdb_init;

    pa_log_info("Initializing \"acdb\" module");

    pa_assert(m);

    m->userdata = u = pa_xnew0(struct userdata,1);
    u->core = m->core;
    u->module = m;

    if(!(u->modargs = pa_modargs_new(m->argument, valid_modargs))){
        pa_log("Failed to parse module arguments.");
        goto fail;
    }

    u->acdb_handle = dlopen(LIB_ACDB_LOADER,RTLD_NOW);
    if(u->acdb_handle ==NULL){
        pa_log("%s:DLOPEN failed for %s, error is: %s", __func__, LIB_ACDB_LOADER, dlerror());
        goto fail;
    }
    else
        pa_log_info("%s:DLOPEN successful for %s", __func__, LIB_ACDB_LOADER);

    u->acdb_init = (acdb_init_t)dlsym(u->acdb_handle, "acdb_loader_init_v2");
    if(!u->acdb_init)
        pa_log("%s:could not find the symbol acdb_loader_init from %s", __func__, LIB_ACDB_LOADER);

    ret_acdb_init = u->acdb_init(NULL, NULL, 0);
    if(ret_acdb_init)
        pa_log("%s: Error initializing ACDB returned = %d",__func__, ret_acdb_init);
        /* know issue: acdb_init fail because allocate_memory failed. */
    else
        pa_log_info("%s:Initialize ACDB successful!", __func__);

    u->acdb_send_audio_cal = (acdb_send_audio_cal_t)dlsym(u->acdb_handle, "acdb_loader_send_audio_cal_v2");
    if(!u->acdb_send_audio_cal)
        pa_log("%s:could not find the symbol acdb_send_audio_cal from %s", __func__, LIB_ACDB_LOADER);

    u->acdb_deallocate = (acdb_deallocate_t)dlsym(u->acdb_handle, "acdb_loader_deallocate_ACDB");
    if(!u->acdb_deallocate)
        pa_log("%s:could not find the symbol acdb_loader_deallocate_ACDB from %s", __func__, LIB_ACDB_LOADER);

    u->sink_input_put_hook_slot = pa_hook_connect(&m->core->hooks[PA_CORE_HOOK_SINK_INPUT_PUT], PA_HOOK_EARLY, (pa_hook_cb_t) sink_input_put_hook_callback, u);

    u->source_output_put_hook_slot = pa_hook_connect(&m->core->hooks[PA_CORE_HOOK_SOURCE_OUTPUT_PUT], PA_HOOK_EARLY, (pa_hook_cb_t) source_output_put_hook_callback, u);

    return 0;

fail:
    return -1;
}

void pa__done(pa_module* m)
{
    struct userdata *u;

    pa_assert(m);

    if(u = m->userdata){

        if(u->modargs)
            pa_modargs_free(u->modargs);

        if(u->sink_input_put_hook_slot)
            pa_hook_slot_free(u->sink_input_put_hook_slot);

        if(u->source_output_put_hook_slot)
            pa_hook_slot_free(u->source_output_put_hook_slot);

        if(u->acdb_deallocate) {
            pa_log_info("%s:deallocating for ACDB ...", __func__);
            u->acdb_deallocate();
	}

        if(u->acdb_handle){
            dlclose(u->acdb_handle);
            pa_log_info("%s:DLCLOSE successful for %s", __func__, LIB_ACDB_LOADER);
        }
    }

    pa_xfree(u);

    pa_log_info("Closing \"acdb\" module");
}
